import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import cv2
import random
import keras
from keras.models import Model

train_df = pd.read_csv('/content/drive/MyDrive/cloud_project/train.csv')
print(train_df.shape)
train_df.head(10)

train_image_folder='/content/drive/MyDrive/cloud_project/train_images'
print("No of trainImages:",len(os.listdir(train_image_folder)))

train_df_drop=train_df
train_df_drop=train_df_drop.dropna()
train_df_drop.shape

train_df['ImageId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[0])
train_df['ClassId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[1])
train_df['hasMask'] = ~ train_df['EncodedPixels'].isna()

print(train_df.shape)
train_df.head()

class_count=train_df.groupby('ClassId').count()
Fish_count=class_count['EncodedPixels'][0]
Flower_count=class_count['EncodedPixels'][1]
Gravel_count=class_count['EncodedPixels'][2]
Sugar_count=class_count['EncodedPixels'][3]
print('No of Fish mask:',Fish_count)
print('No of Flower mask:',Flower_count)
print('No of Gravel mask:',Gravel_count)
print('No of Sugar mask:',Sugar_count)

# plotting a pie chart
labels = 'Fish', 'Flower', 'Gravel', 'Sugar'
sizes = [Fish_count, Flower_count, Gravel_count, Sugar_count]
colors = ( "#c70039", "#ff5733", "#ffc305", "#009688") 

fig, ax = plt.subplots(figsize=(6, 6),dpi=100)
ax.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90, colors=colors)
ax.axis('equal')
ax.set_title('Cloud Types')

plt.show()

mask_count_df = train_df.groupby('ImageId').agg(np.sum).reset_index()
mask_count_df.sort_values('hasMask', ascending=False, inplace=True)
print(mask_count_df.shape)
mask_count_df.head()

maskedSamples = train_df[train_df['hasMask'] == True]
maskedSamples_gp = maskedSamples.groupby('ImageId').size().reset_index(name='Number of masks')
for n_masks in np.sort(maskedSamples_gp['Number of masks'].unique()):
    print('Samples with %s masks: %s' % (n_masks, len(maskedSamples_gp[maskedSamples_gp['Number of masks'] == n_masks])))

f, ax = plt.subplots(figsize=(18, 6))
ax = sns.countplot(y="Number of masks", data=maskedSamples_gp, palette="rocket")
sns.despine()
plt.show()

sns.set_style("white")
plt.figure(figsize=[60, 20])
for index, row in train_df[:8].iterrows():
    img = cv2.imread("/content/drive/MyDrive/cloud_project/train_images/%s" % row['ImageId'])[...,[2, 1, 0]]
    plt.subplot(2, 4, index+1)
    plt.imshow(img)
    plt.title("Image %s - Label %s" % (index, row['ClassId']), fontsize=22)
    plt.axis('off')    
    
plt.show()

class_names = ['Fish', 'Flower','Sugar', 'Gravel']

def rle_decode(mask, shape=(1400,2100)) :
    m = str(mask)
    if m == 'nan' :
        m = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    else :
        m = m.split()
        starts = np.asarray(m[0:][::2], dtype=int) - 1
        lengths = np.asarray(m[1:][::2], dtype=int)
        ends = starts + lengths
        
        m = np.zeros(shape[0]*shape[1], dtype=np.uint8)
        for lo, hi in zip(starts, ends):
            m[lo:hi] = 1
   
    m = m.reshape(shape, order='F')
    #m = cv.resize(m, resized_inv)
    return m


def make_mask(df, image_label, shape = (1400, 2100), cv_shape = (2100, 1400),debug=False):
    """
    Create mask based on scv file, Image_Label and shape.
    """
    if debug:
        print(shape,cv_shape)
    df = df.set_index('Image_Label')
    encoded_mask = df.loc[image_label, 'EncodedPixels']
    mask = np.zeros((shape[0], shape[1]), dtype=np.float32)
    if encoded_mask is not np.nan:
        mask = rle_decode(encoded_mask,shape=shape) # original size
        
    return cv2.resize(mask, cv_shape)


def draw_masks(img2,img_mask_list):
    
    img = img2.copy()
    for ii in range(4): # for each of the 4 masks
        color_mask = np.zeros(img2.shape)
        temp_mask = np.ones([img2.shape[0],img2.shape[1]])*127./255.
        temp_mask[img_mask_list[ii] == 0] = 0
        if ii < 3: # use different color for each mask
            color_mask[:,:,ii] = temp_mask
        else:
            color_mask[:,:,0],color_mask[:,:,1],color_mask[:,:,2] = temp_mask,temp_mask,temp_mask # broadcasting to 3 channels
    
        img += color_mask
        
    return img

def draw_one_mask(img2,img_mask_list):
    
    img = img2.copy()
    for ii in range(1): # for each of the 4 masks
        color_mask = np.zeros(img2.shape)
        temp_mask = np.ones([img2.shape[0],img2.shape[1]])*127./255.
        temp_mask[img_mask_list[ii] == 0] = 0
        color_mask[:,:,ii] = temp_mask
    
        img += color_mask
        
    return img

def show_image(image,title=None,figsize=None):
    
    if figsize is not None:
        fig = plt.figure(figsize=figsize)
        
    if image.ndim == 2:
        plt.imshow(image,cmap='gray')
    else:
        plt.imshow(image)
        
    if title is not None:
        plt.title(title)
        

def show_Nimages(imgs,tags,size=(5,4),scale=1):

    N=len(imgs)
    fig = plt.figure(figsize=size)
    for i, img in enumerate(imgs):
        ax = fig.add_subplot(1, N, i + 1, xticks=[], yticks=[])
        show_image(img,title=tags[i])
    plt.show()

NN=4
image_label_list=train_df.Image_Label.tolist()
image_id_list=[]
images_list = []

for i in image_label_list:
    image_id_list.append(i.split('_')[0])
    
temp_image_id_list=list(set(image_id_list))  

images_list = random.sample(temp_image_id_list, 4)

current_batch = images_list[0: NN]

for i, image_name in enumerate(current_batch):
   
    # p=f'{path}/train_images/'
    p='/content/drive/MyDrive/cloud_project/train_images/'
    path_im=p+image_name; 
    img = cv2.imread(path_im).astype(np.float32)
    #print(img.max(), img.min()) keep this
    img = img/255.
    
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img2 = np.stack([img2,img2,img2],axis=-1)
    
    img_mask=[]
    temp_mask=[]
    img_mask_convex=[]
    img_mask_convex_minsize=[]
    container=[]
    tag=[]
    
    show_Nimages([img2],['original_image '+str(i+1)],size=(5,4)) 

    for class_id in range(4):
        img_mask.append(make_mask(train_df, image_name + '_' + class_names[class_id],shape=(1400,2100)))
        temp_mask=[make_mask(train_df, image_name + '_' + class_names[class_id],shape=(1400,2100))]
        img_temp=draw_one_mask(img2,temp_mask)
        container.append(img_temp)
        tag.append(class_names[class_id])

    show_Nimages(container,tag,size=(20,16))
    img3 = draw_masks(img2,img_mask)
   
    show_Nimages([img3],['full_mask_image '+str(i+1)],size=(5,4))

history_df = pd.read_csv('/content/drive/MyDrive/MinorProject/history.csv')
history_df[['loss', 'val_loss']].plot(figsize=(10,10),xlabel="epoch",ylabel="loss")

history_df = pd.read_csv('/content/drive/MyDrive/MinorProject/history.csv')
history_df[['dice_coef', 'val_dice_coef']].plot(figsize=(10,10),xlabel="epoch",ylabel="dice coefficient")

history_df = pd.read_csv('/content/drive/MyDrive/MinorProject/history.csv')
history_df[['lr']].plot(figsize=(10,10),xlabel="epoch",ylabel="learning rate")

model_saved = keras.models.load_model('/content/drive/MyDrive/MinorProject/model.h5',compile=False)
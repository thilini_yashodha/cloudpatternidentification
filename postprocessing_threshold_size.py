import os
import json
import albumentations as albu
import cv2
import keras
from keras import backend as K
from keras.models import Model
from keras.layers import Input
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras.losses import binary_crossentropy
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from skimage.exposure import adjust_gamma
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split
# %env SM_FRAMEWORK=tf.keras
import segmentation_models as sm
import tensorflow as tf
tf.config.run_functions_eagerly(True)
import seaborn as sns
import random

train_df = pd.read_csv('/content/drive/MyDrive/cloud_project/train.csv')
train_image_folder='/content/drive/MyDrive/cloud_project/train_images'
print("No of trainImages:",len(os.listdir(train_image_folder)))
train_df['ImageId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[0])
train_df['ClassId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[1])
train_df['hasMask'] = ~ train_df['EncodedPixels'].isna()
mask_count_df = train_df.groupby('ImageId').agg(np.sum).reset_index()
mask_count_df.sort_values('hasMask', ascending=False, inplace=True)
print(mask_count_df.shape)
mask_count_df.head()

def np_resize(img, input_shape):
    """
    Reshape a numpy array, which is input_shape=(height, width), 
    as opposed to input_shape=(width, height) for cv2
    """
    height, width = input_shape
    return cv2.resize(img, (width, height))
    
def mask2rle(img):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    pixels= img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)

def rle2mask(rle, input_shape):
    width, height = input_shape[:2]
    
    mask= np.zeros( width*height ).astype(np.uint8)
    
    array = np.asarray([int(x) for x in rle.split()])
    starts = array[0::2]
    lengths = array[1::2]

    current_position = 0
    for index, start in enumerate(starts):
        mask[int(start):int(start+lengths[index])] = 1
        current_position += lengths[index]
        
    return mask.reshape(height, width).T

def build_masks(rles, input_shape, reshape=None):
    depth = len(rles)
    if reshape is None:
        masks = np.zeros((*input_shape, depth))
    else:
        masks = np.zeros((*reshape, depth))
    
    for i, rle in enumerate(rles):
        if type(rle) is str:
            if reshape is None:
                masks[:, :, i] = rle2mask(rle, input_shape)
            else:
                mask = rle2mask(rle, input_shape)
                reshaped_mask = np_resize(mask, reshape)
                masks[:, :, i] = reshaped_mask
    
    return masks

def build_rles(masks, reshape=None):
    width, height, depth = masks.shape
    
    rles = []
    
    for i in range(depth):
        mask = masks[:, :, i]
        
        if reshape:
            mask = mask.astype(np.float32)
            mask = np_resize(mask, reshape).astype(np.int64)
        
        rle = mask2rle(mask)
        rles.append(rle)
        
    return rles

def dice_coef(y_true, y_pred, smooth=1):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    y_true_f= tf.cast(y_true_f, dtype='float32')
    y_pred_f= tf.cast(y_pred_f, dtype='float32')
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

def dice_loss(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    y_true_f= tf.cast(y_true_f, dtype='float32')
    y_pred_f= tf.cast(y_pred_f, dtype='float32')
    intersection = y_true_f * y_pred_f
    score = (2. * K.sum(intersection) + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return 1. - score

def bce_dice_loss(y_true, y_pred):
    y_true_f= tf.cast(y_true, dtype='float32')
    y_pred_f= tf.cast(y_pred, dtype='float32')
    return binary_crossentropy(y_true_f, y_pred_f) + dice_loss(y_true_f, y_pred_f)

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, df, target_df=None, mode='fit',
                 base_path='/content/drive/MyDrive/cloud_project/train_images/',
                 batch_size=32, dim=(1400, 2100), n_channels=3, reshape=None, gamma=None,
                 augment=False, n_classes=4, random_state=2019, shuffle=True):
        self.dim = dim
        self.batch_size = batch_size
        self.df = df
        self.mode = mode
        self.base_path = base_path
        self.target_df = target_df
        self.list_IDs = list_IDs
        self.reshape = reshape
        self.gamma = gamma
        self.n_channels = n_channels
        self.augment = augment
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.random_state = random_state
        
        self.on_epoch_end()
        np.random.seed(self.random_state)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_batch = [self.list_IDs[k] for k in indexes]
        
        X = self.__generate_X(list_IDs_batch)
        
        if self.mode == 'fit':
            y = self.__generate_y(list_IDs_batch)
            
            if self.augment:
                X, y = self.__augment_batch(X, y)

            return X, y
        
        elif self.mode == 'predict':
            return X

        else:
            raise AttributeError('The mode parameter should be set to "fit" or "predict".')
        
    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.seed(self.random_state)
            np.random.shuffle(self.indexes)
    
    def __generate_X(self, list_IDs_batch):
        'Generates data containing batch_size samples'
        # Initialization
        if self.reshape is None:
            X = np.empty((self.batch_size, *self.dim, self.n_channels))

        else:
            X = np.empty((self.batch_size, *self.reshape, self.n_channels))
        
        # Generate data
        for i, ID in enumerate(list_IDs_batch):
            im_name = self.df['ImageId'].iloc[ID]
            img_path = self.base_path + im_name
            img = self.__load_rgb(img_path)
            
            if self.reshape is not None:
                img = np_resize(img, self.reshape)
            
            # Adjust gamma
            if self.gamma is not None:
                img = adjust_gamma(img, gamma=self.gamma)
            
            # Store samples
            X[i,] = img

        return X

    def __generate_y(self, list_IDs_batch):
        if self.reshape is None:
            y = np.empty((self.batch_size, *self.dim, self.n_classes), dtype=int)
        else:
            y = np.empty((self.batch_size, *self.reshape, self.n_classes), dtype=int)
        
        for i, ID in enumerate(list_IDs_batch):
            im_name = self.df['ImageId'].iloc[ID]
            image_df = self.target_df[self.target_df['ImageId'] == im_name]
            
            rles = image_df['EncodedPixels'].values
            
            if self.reshape is not None:
                masks = build_masks(rles, input_shape=self.dim, reshape=self.reshape)
            else:
              masks = build_masks(rles, input_shape=self.dim)
            
            y[i, ] = masks

        return y
    
    def __load_grayscale(self, img_path):
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        img = img.astype(np.float32) / 255.
        img = np.expand_dims(img, axis=-1)

        return img
    
    def __load_rgb(self, img_path):
        img1 = cv2.imread(img_path)
        img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
        img1 = img1.astype(np.float32) / 255.

        return img1

    def __random_transform(self, img, masks):
        composition = albu.Compose([
            albu.HorizontalFlip(),
            albu.VerticalFlip(),
            albu.ShiftScaleRotate(rotate_limit=30, shift_limit=0.1)
        ])
        
        composed = composition(image=img, mask=masks)
        aug_img = composed['image']
        aug_masks = composed['mask']
        
        return aug_img, aug_masks
    
    def __augment_batch(self, img_batch, masks_batch):
        for i in range(img_batch.shape[0]):
            img_batch[i, ], masks_batch[i, ] = self.__random_transform(
                img_batch[i, ], masks_batch[i, ])
        
        return img_batch, masks_batch

from keras import optimizers
opt = optimizers.Adam(lr=0.002)

def post_process(probability, threshold, min_size):
    """
    Post processing of each predicted mask, components with lesser number of pixels
    than `min_size` are ignored
    """
    
    mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
    
    num_component, component = cv2.connectedComponents(mask.astype(np.uint8))
    predictions = np.zeros((350, 525), np.float32)
    num = 0
    for c in range(1, num_component):
        p = (component == c)
        if p.sum() > min_size:
            predictions[p] = 1
            num += 1
    return predictions, num

def eval_score(pred, true,eps=0.00000001):
    pred = np.asarray(pred).astype(np.bool)
    true = np.asarray(true).astype(np.bool)

    intersection = np.logical_and(pred, true)

    return (2.0 * intersection.sum()) / (pred.sum() + true.sum()), (intersection.sum()+eps) / (pred.sum()+eps) , (intersection.sum()+eps) / (true.sum()+ eps)

model_saved = keras.models.load_model('/content/drive/MyDrive/cloud_project/model11_folder/model13.h5',compile=False)

model_saved.compile(optimizer=opt, loss=bce_dice_loss, metrics=[dice_coef])

model_saved.summary()

train_imgs=pd.DataFrame(train_df['ImageId'].unique(), columns=['ImageId'])
val_train_df = pd.read_csv('/content/drive/MyDrive/cloud_project/train.csv')
val_train_df['ImageId'] = val_train_df['Image_Label'].apply(lambda x: x.split('_')[0])

predict_mask_list=[]
val_encoded_pixels=[]

val_predict_generator = DataGenerator(
        val_idx,
        df=mask_count_df,
        shuffle=False,
        mode='predict',
        dim=(350, 525),
        reshape=(320, 480),
        n_channels=3,
        gamma=0.8,
        base_path='/content/drive/MyDrive/cloud_project/train_images/',
        target_df=train_df,
        batch_size=1,
        n_classes=4
    )

val_batch_pred_masks = model_saved.predict_generator(
        val_predict_generator, 
        workers=1,
        verbose=1
    ) 

atm0=[]
atm1=[]
atm2=[]
atm3=[]

for threshold in range(0, 100, 5):
      threshold /= 100
      for size in [500, 1000, 5000,7500, 10000, 15000]:
          
          d0=[]
          d1=[]
          d2=[]
          d3=[]

          for i in range(val_batch_pred_masks.shape[0]):
                pred_id_mask=val_batch_pred_masks[i]

                index=val_idx[i]
                pointer=mask_count_df.iloc[index]['ImageId']
                sample_df = train_df[train_df['ImageId'] == pointer]
              
                for j in range(4):
                      pred_mask=pred_id_mask[:,:,j]
                      if pred_mask.shape != (350, 525):
                          pred_mask = cv2.resize(pred_mask, dsize=(525, 350), interpolation=cv2.INTER_LINEAR)

                      f_pred_mask, num_predict = post_process(pred_mask, threshold, size)

                      mask=sample_df['EncodedPixels'].values[j]
                      if (type(mask)== str ):
                            ms=rleCONVERTmask(mask, (2100,1400))
                      else:
                            ms=rleCONVERTmask("", (2100,1400))

                      if ms != (350, 525):
                          m = cv2.resize(ms, dsize=(525, 350), interpolation=cv2.INTER_LINEAR)

                      if (j==0):
                        d=d0
                      elif (j==1):
                        d=d1
                      elif (j==2):
                        d=d2
                      elif (j==3):
                        d=d3

                      if (f_pred_mask.sum() == 0) & (m.sum() == 0):
                            d.append((1,1,1))
                            
                      else:
                            dice,precision,recall=eval_score(f_pred_mask, m)
                            # x=dice(f_pred_mask,m)
                            d.append((dice,precision,recall))
                            # print("dice:",x)
                            # print("dice:",dice," ","precision:",precision," ","recall:",recall)
      
          atm0.append((threshold, size, np.mean(d0,axis = 0)[0],np.mean(d0,axis = 0)[1],np.mean(d0,axis = 0)[2]))
          atm1.append((threshold, size, np.mean(d1,axis = 0)[0],np.mean(d1,axis = 0)[1],np.mean(d1,axis = 0)[2]))
          atm2.append((threshold, size, np.mean(d2,axis = 0)[0],np.mean(d2,axis = 0)[1],np.mean(d2,axis = 0)[2]))
          atm3.append((threshold, size, np.mean(d3,axis = 0)[0],np.mean(d3,axis = 0)[1],np.mean(d3,axis = 0)[2]))

attempt0=sorted(atm0, key=lambda x: x[2], reverse=True)
fish_attempts_df = pd.DataFrame(attempt0, columns=['threshold', 'size', 'dice', 'precision','recall'])
print(fish_attempts_df.head())

attempt1=sorted(atm1, key=lambda x: x[2], reverse=True)
flower_attempts_df = pd.DataFrame(attempt1, columns=['threshold', 'size', 'dice', 'precision','recall'])
print(flower_attempts_df.head())

attempt2=sorted(atm2, key=lambda x: x[2], reverse=True)
gravel_attempts_df = pd.DataFrame(attempt2, columns=['threshold', 'size', 'dice', 'precision','recall'])
print(gravel_attempts_df.head())

attempt3=sorted(atm3, key=lambda x: x[2], reverse=True)
sugar_attempts_df = pd.DataFrame(attempt3, columns=['threshold', 'size', 'dice', 'precision','recall'])
print(sugar_attempts_df.head())

all=[]
all.append(("Fish",attempt0[0][2],attempt0[0][3],attempt0[0][4]))
all.append(("Flower",attempt1[0][2],attempt1[0][3],attempt1[0][4]))
all.append(("Gravel",attempt2[0][2],attempt2[0][3],attempt2[0][4]))
all.append(("Sugar",attempt3[0][2],attempt3[0][3],attempt3[0][4]))
for i in range(4):
   print(all[i])

overall_attempts=[]
for i in range(len(atm0)):
  cal_dice=(atm0[i][2]+atm1[i][2]+atm2[i][2]+atm3[i][2])/4
  cal_precision=(atm0[i][3]+atm1[i][3]+atm2[i][3]+atm3[i][3])/4
  cal_recall=(atm0[i][4]+atm1[i][4]+atm2[i][4]+atm3[i][4])/4
  overall_attempts.append((atm0[i][0],atm0[i][1],cal_dice,cal_precision,cal_recall))

sorted_overall_attempts=sorted(overall_attempts, key=lambda x: x[2], reverse=True)
overall_attempts_df = pd.DataFrame(sorted_overall_attempts, columns=['threshold', 'size', 'dice', 'precision','recall'])
overall_attempts_df.head()

sns.lineplot(x='threshold', y='dice', hue='size', data=overall_attempts_df);
plt.title('Threshold and min size vs dice');
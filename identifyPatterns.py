import os
import json
import albumentations as albu
import cv2
import keras
from keras import backend as K
from keras.models import Model
from keras.layers import Input
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate
from keras.losses import binary_crossentropy
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from skimage.exposure import adjust_gamma
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split
# %env SM_FRAMEWORK=tf.keras
import segmentation_models as sm
import tensorflow as tf
tf.config.run_functions_eagerly(True)
import seaborn as sns
import random

train_df = pd.read_csv('/content/drive/MyDrive/cloud_project/train.csv')
train_image_folder='/content/drive/MyDrive/cloud_project/train_images'
print("No of trainImages:",len(os.listdir(train_image_folder)))
train_df['ImageId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[0])
train_df['ClassId'] = train_df['Image_Label'].apply(lambda x: x.split('_')[1])
train_df['hasMask'] = ~ train_df['EncodedPixels'].isna()
mask_count_df = train_df.groupby('ImageId').agg(np.sum).reset_index()
mask_count_df.sort_values('hasMask', ascending=False, inplace=True)
print(mask_count_df.shape)
mask_count_df.head()

sub_df = pd.read_csv('/content/drive/MyDrive/cloud_project/sample_submission.csv')
sub_df['ImageId'] = sub_df['Image_Label'].apply(lambda x: x.split('_')[0])
test_imgs = pd.DataFrame(sub_df['ImageId'].unique(), columns=['ImageId'])

test_imgs.head()

sub_df.head()

def np_resize(img, input_shape):
    """
    Reshape a numpy array, which is input_shape=(height, width), 
    as opposed to input_shape=(width, height) for cv2
    """
    height, width = input_shape
    return cv2.resize(img, (width, height))
    
def mask2rle(img):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    pixels= img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)

def rle2mask(rle, input_shape):
    width, height = input_shape[:2]
    
    mask= np.zeros( width*height ).astype(np.uint8)
    
    array = np.asarray([int(x) for x in rle.split()])
    starts = array[0::2]
    lengths = array[1::2]

    current_position = 0
    for index, start in enumerate(starts):
        mask[int(start):int(start+lengths[index])] = 1
        current_position += lengths[index]
        
    return mask.reshape(height, width).T

def build_masks(rles, input_shape, reshape=None):
    depth = len(rles)
    if reshape is None:
        masks = np.zeros((*input_shape, depth))
    else:
        masks = np.zeros((*reshape, depth))
    
    for i, rle in enumerate(rles):
        if type(rle) is str:
            if reshape is None:
                masks[:, :, i] = rle2mask(rle, input_shape)
            else:
                mask = rle2mask(rle, input_shape)
                reshaped_mask = np_resize(mask, reshape)
                masks[:, :, i] = reshaped_mask
    
    return masks

def build_rles(masks, reshape=None):
    width, height, depth = masks.shape
    
    rles = []
    
    for i in range(depth):
        mask = masks[:, :, i]
        
        if reshape:
            mask = mask.astype(np.float32)
            mask = np_resize(mask, reshape).astype(np.int64)
        
        rle = mask2rle(mask)
        rles.append(rle)
        
    return rles

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, df, target_df=None, mode='fit',
                 base_path='/content/drive/MyDrive/cloud_project/train_images/',
                 batch_size=32, dim=(1400, 2100), n_channels=3, reshape=None, gamma=None,
                 augment=False, n_classes=4, random_state=2019, shuffle=True):
        self.dim = dim
        self.batch_size = batch_size
        self.df = df
        self.mode = mode
        self.base_path = base_path
        self.target_df = target_df
        self.list_IDs = list_IDs
        self.reshape = reshape
        self.gamma = gamma
        self.n_channels = n_channels
        self.augment = augment
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.random_state = random_state
        
        self.on_epoch_end()
        np.random.seed(self.random_state)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_batch = [self.list_IDs[k] for k in indexes]
        
        X = self.__generate_X(list_IDs_batch)
        
        if self.mode == 'fit':
            y = self.__generate_y(list_IDs_batch)
            
            if self.augment:
                X, y = self.__augment_batch(X, y)

            return X, y
        
        elif self.mode == 'predict':
            return X

        else:
            raise AttributeError('The mode parameter should be set to "fit" or "predict".')
        
    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.seed(self.random_state)
            np.random.shuffle(self.indexes)
    
    def __generate_X(self, list_IDs_batch):
        'Generates data containing batch_size samples'
        # Initialization
        if self.reshape is None:
            X = np.empty((self.batch_size, *self.dim, self.n_channels))

        else:
            X = np.empty((self.batch_size, *self.reshape, self.n_channels))
        
        # Generate data
        for i, ID in enumerate(list_IDs_batch):
            im_name = self.df['ImageId'].iloc[ID]
            img_path = self.base_path + im_name
            img = self.__load_rgb(img_path)
            
            if self.reshape is not None:
                img = np_resize(img, self.reshape)
            
            # Adjust gamma
            if self.gamma is not None:
                img = adjust_gamma(img, gamma=self.gamma)
            
            # Store samples
            X[i,] = img

        return X

    def __generate_y(self, list_IDs_batch):
        if self.reshape is None:
            y = np.empty((self.batch_size, *self.dim, self.n_classes), dtype=int)
        else:
            y = np.empty((self.batch_size, *self.reshape, self.n_classes), dtype=int)
        
        for i, ID in enumerate(list_IDs_batch):
            im_name = self.df['ImageId'].iloc[ID]
            image_df = self.target_df[self.target_df['ImageId'] == im_name]
            
            rles = image_df['EncodedPixels'].values
            
            if self.reshape is not None:
                masks = build_masks(rles, input_shape=self.dim, reshape=self.reshape)
            else:
              masks = build_masks(rles, input_shape=self.dim)
            
            y[i, ] = masks

        return y
    
    def __load_grayscale(self, img_path):
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        img = img.astype(np.float32) / 255.
        img = np.expand_dims(img, axis=-1)

        return img
    
    def __load_rgb(self, img_path):
        img1 = cv2.imread(img_path)
        img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
        img1 = img1.astype(np.float32) / 255.

        return img1

    def __random_transform(self, img, masks):
        composition = albu.Compose([
            albu.HorizontalFlip(),
            albu.VerticalFlip(),
            albu.ShiftScaleRotate(rotate_limit=30, shift_limit=0.1)
        ])
        
        composed = composition(image=img, mask=masks)
        aug_img = composed['image']
        aug_masks = composed['mask']
        
        return aug_img, aug_masks
    
    def __augment_batch(self, img_batch, masks_batch):
        for i in range(img_batch.shape[0]):
            img_batch[i, ], masks_batch[i, ] = self.__random_transform(
                img_batch[i, ], masks_batch[i, ])
        
        return img_batch, masks_batch

model_saved = keras.models.load_model('/content/drive/MyDrive/cloud_project/model11_folder/model13.h5',compile=False)

def post_process(probability, threshold, min_size):
    """
    Post processing of each predicted mask, components with lesser number of pixels
    than `min_size` are ignored
    """
    
    mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
    
    num_component, component = cv2.connectedComponents(mask.astype(np.uint8))
    predictions = np.zeros((350, 525), np.float32)
    num = 0
    for c in range(1, num_component):
        p = (component == c)
        if p.sum() > min_size:
            predictions[p] = 1
            num += 1
    return predictions, num

def eval_score(pred, true,eps=0.00000001):
    pred = np.asarray(pred).astype(np.bool)
    true = np.asarray(true).astype(np.bool)

    intersection = np.logical_and(pred, true)

    return (2.0 * intersection.sum()) / (pred.sum() + true.sum()), (intersection.sum()+eps) / (pred.sum()+eps) , (intersection.sum()+eps) / (true.sum()+ eps)

def rleCONVERTmask(r, input_shape):
    rle=str(r)
    width, height = input_shape[:2]
    mask = np.zeros( width*height ).astype(np.uint8)
    array = np.asarray([int(float(x)) for x in rle.split()])
    starts = array[0::2]
    lengths = array[1::2]

    current_position = 0
    for index, start in enumerate(starts):
        mask[int(start):int(start+lengths[index])] = 1
        current_position += lengths[index]
        
    return mask.reshape(height, width).T

test_df = []
encoded_pixels = []
TEST_BATCH_SIZE = 500

for i in range(0, test_imgs.shape[0], TEST_BATCH_SIZE):
    batch_idx = list(
        range(i, min(test_imgs.shape[0], i + TEST_BATCH_SIZE))
    )

    test_generator = DataGenerator(
        batch_idx,
        df=test_imgs,
        shuffle=False,
        mode='predict',
        dim=(350, 525),
        reshape=(320, 480),
        n_channels=3,
        gamma=0.8,
        base_path='/content/drive/MyDrive/cloud_project/test_images/',
        target_df=sub_df,
        batch_size=1,
        n_classes=4
    )

    batch_pred_masks = model_saved.predict_generator(
        test_generator, 
        workers=1,
        verbose=1
    ) 

    # Predict out put shape is (320X480X4)
    # 4  = 4 classes, Fish, Flower, Gravel Surger.
    
    for j, idx in enumerate(batch_idx):
        filename = test_imgs['ImageId'].iloc[idx]
        image_df = sub_df[sub_df['ImageId'] == filename].copy()
        
        # Batch prediction result set
        pred_masks = batch_pred_masks[j, ]
        
        for k in range(pred_masks.shape[-1]):
            pred_mask = pred_masks[...,k].astype('float32') 
            
            if pred_mask.shape != (350, 525):
                pred_mask = cv2.resize(pred_mask, dsize=(525, 350), interpolation=cv2.INTER_LINEAR)

            threshold=0.45
            min_size=15000

            pred_mask, num_predict = post_process(pred_mask, threshold, min_size)
            
            if num_predict == 0:
                encoded_pixels.append('')
            else:
                r = mask2rle(pred_mask)
                encoded_pixels.append(r)
                
sub_df['EncodedPixels'] = encoded_pixels

test_df = pd.read_csv('/content/drive/MyDrive/cloud_project/model11_folder/submission_model13.csv')
test_df.head()

test_df['ImageId'] = test_df['Image_Label'].apply(lambda x: x.split('_')[0])
test_df['ClassId'] = test_df['Image_Label'].apply(lambda x: x.split('_')[1])
test_df['hasMask'] = ~ test_df['EncodedPixels'].isna()
test_df.head()

# Segmentation related
def decode(mask_rle, shape=(1400, 2100)):
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape, order='F')  # Needed to align to RLE direction


def rleTOmask(r, input_shape):
    rle=str(r)
    width, height = input_shape[:2]
    mask = np.zeros( width*height ).astype(np.uint8)
    array = np.asarray([int(float(x)) for x in rle.split()])
    starts = array[0::2]
    lengths = array[1::2]

    current_position = 0
    for index, start in enumerate(starts):
        mask[int(start):int(start+lengths[index])] = 1
        current_position += lengths[index]
        
    return mask.reshape(height, width).T

def inspect_predictions(df, image_ids, images_dest_path,img_shape,label_col='EncodedPixels', title_col='Image_Label',  figsize=(22, 6)):
        for sample in image_ids:
            sample_df = df[df['ImageId'] == sample]
            fig, axes = plt.subplots(1, 5, figsize=figsize)
            img = cv2.imread(images_dest_path + sample_df['ImageId'].values[0])
            img = cv2.resize(img, img_shape)
            axes[0].imshow(img)
            axes[0].set_title(('{}_Original'.format(sample_df['ImageId'].values[0])), fontsize=16)
            axes[0].axis('off')
            for i in range(4):
                if (type(sample_df[label_col].values[i])== str ):
                  axes[i+1].imshow(rleTOmask(sample_df[label_col].values[i], img.shape))
                else:
                  axes[i+1].imshow(rleTOmask("", img.shape))
                axes[i+1].set_title(sample_df[title_col].values[i], fontsize=18)
                axes[i+1].axis('off')

images_to_inspect=random.choices(val_idx, k=5)

test_images_dest_path='/content/drive/MyDrive/cloud_project/train_images/'

inspect_generator = DataGenerator(
        images_to_inspect,
        df=mask_count_df,
        shuffle=False,
        mode='predict',
        dim=(350, 525),
        reshape=(320, 480),
        n_channels=3,
        gamma=0.8,
        base_path='/content/drive/MyDrive/cloud_project/train_images/',
        target_df=train_df,
        batch_size=1,
        n_classes=4
    )

preds = model_saved.predict_generator(
        inspect_generator, 
        workers=1,
        verbose=1
    ) 



for i in range(preds.shape[0]):
     pred_id_mask=preds[i]

     pointer=mask_count_df.iloc[images_to_inspect[i]]['ImageId']
     images_to_inspect_val=[]
     images_to_inspect_val.append(pointer)
     inspect_predictions(train_df, images_to_inspect_val, test_images_dest_path,(2100, 1400))
  
     fig, axes = plt.subplots(1, 5, figsize=(22,6))

     for j in range(4):
        pred_mask=pred_id_mask[:,:,j]
        if pred_mask.shape != (350, 525):
              pred_mask = cv2.resize(pred_mask, dsize=(525, 350), interpolation=cv2.INTER_LINEAR)

        threshold=0.45
        size=10000
        p_pred_mask, num_predict = post_process(pred_mask, threshold, size)

        img = cv2.imread(test_images_dest_path + mask_count_df.iloc[images_to_inspect[i]]['ImageId'])
        img = cv2.resize(img, (525,350))
        axes[0].imshow(img)
        axes[0].set_title('{}_Original'.format(mask_count_df.iloc[images_to_inspect[i]]['ImageId']), fontsize=16)
        axes[0].axis('off')

        axes[j+1].imshow(p_pred_mask)
        
        if j==0:
          tail='Fish'
        elif j==1:
          tail='Flower'
        elif j==2:
          tail='Gravel'
        elif j==3:
          tail='Sugar'

        title='{}_{}'.format(mask_count_df.iloc[images_to_inspect[i]]['ImageId'],tail)
        axes[j+1].set_title(title, fontsize=18)
        axes[j+1].axis('off')

# Choose 5 samples at random
test_images_dest_path='/content/drive/MyDrive/cloud_project/test_images/'
images_to_inspect_test =  np.random.choice(sub['ImageId'].unique(), 4, replace=False)
print(images_to_inspect_test)
inspect_predictions(test_df, images_to_inspect_test, test_images_dest_path, (525,350))

print(test_df.shape)

test_df_drop=sub
test_df_drop=test_df_drop.dropna()
test_df_drop.shape

class_count_test=test_df.groupby('ClassId').count()
Fish_count_test=class_count_test['EncodedPixels'][0]
Flower_count_test=class_count_test['EncodedPixels'][1]
Gravel_count_test=class_count_test['EncodedPixels'][2]
Sugar_count_test=class_count_test['EncodedPixels'][3]
print('No of Fish mask:',Fish_count_test)
print('No of Flower mask:',Flower_count_test)
print('No of Gravel mask:',Gravel_count_test)
print('No of Sugar mask:',Sugar_count_test)

# plotting a pie chart
labels = 'Fish', 'Flower', 'Gravel', 'Sugar'
sizes = [Fish_count_test, Flower_count_test, Gravel_count_test, Sugar_count_test]
colors = ( "#c70039", "#ff5733", "#ffc305", "#009688") 

fig, ax = plt.subplots(figsize=(6, 6),dpi=100)
ax.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90, colors=colors)
ax.axis('equal')
ax.set_title('Cloud Types')

plt.show()

test_mask_count_df = test_df.groupby('ImageId').agg(np.sum).reset_index()
test_mask_count_df.sort_values('hasMask', ascending=False, inplace=True)
print(test_mask_count_df.shape)
test_mask_count_df.head()

test_maskedSamples = test_df[test_df['hasMask'] == True]
test_maskedSamples_gp = test_maskedSamples.groupby('ImageId').size().reset_index(name='Number of masks')
for n_masks in np.sort(test_maskedSamples_gp['Number of masks'].unique()):
    print('Samples with %s masks: %s' % (n_masks, len(test_maskedSamples_gp[test_maskedSamples_gp['Number of masks'] == n_masks])))

f, ax = plt.subplots(figsize=(18, 6))
ax = sns.countplot(y="Number of masks", data=test_maskedSamples_gp, palette="rocket")
sns.despine()
plt.show()
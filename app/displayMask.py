import pandas as pd
import random
import cv2
import numpy as np
import matplotlib.pyplot as plt

class Display():

    def function(self):

        test_df = pd.read_csv('ans.csv')
        # print(test_df.head())

        test_df['ImageId'] = test_df['Image_Label'].apply(lambda x: x.split('_')[0])
        test_df['ClassId'] = test_df['Image_Label'].apply(lambda x: x.split('_')[1])
        test_df['hasMask'] = ~ test_df['EncodedPixels'].isna()
        print(test_df.head())

        print(test_df.shape)

        class_names = ['Fish', 'Flower','Sugar', 'Gravel']


        def rle_decode(mask, shape=(350,525)) :
            m = str(mask)
            if m == 'nan' :
                m = np.zeros(shape[0]*shape[1], dtype=np.uint8)
            else :
                m = m.split()
                starts = np.asarray(m[0:][::2], dtype=int) - 1
                lengths = np.asarray(m[1:][::2], dtype=int)
                ends = starts + lengths
                
                m = np.zeros(shape[0]*shape[1], dtype=np.uint8)
                for lo, hi in zip(starts, ends):
                    m[lo:hi] = 1
        
            m = m.reshape(shape, order='F')
            #m = cv.resize(m, resized_inv)
            return m


        def make_mask(df, image_label, shape = (350, 525), cv_shape = (525, 350),debug=False):
            """
            Create mask based on scv file, Image_Label and shape.
            """
            if debug:
                print(shape,cv_shape)
            df = df.set_index('Image_Label')
            encoded_mask = df.loc[image_label, 'EncodedPixels']
            mask = np.zeros((shape[0], shape[1]), dtype=np.float32)
            if encoded_mask is not np.nan:
                mask = rle_decode(encoded_mask,shape=shape) # original size
                
            return cv2.resize(mask, cv_shape)


        def draw_masks(img2,img_mask_list):
            
            img = img2.copy()
            for ii in range(4): # for each of the 4 masks
                color_mask = np.zeros(img2.shape)
                # print('s0 :',img2.shape[0])
                # print('s1 :',img2.shape[1])
                temp_mask = np.ones([img2.shape[0],img2.shape[1]])*127./255.
                temp_mask[img_mask_list[ii] == 0] = 0
                if ii < 3: # use different color for each mask
                    color_mask[:,:,ii] = temp_mask
                else:
                    color_mask[:,:,0],color_mask[:,:,1],color_mask[:,:,2] = temp_mask,temp_mask,temp_mask # broadcasting to 3 channels
            
                img += color_mask
                
            return img

        def draw_one_mask(img2,img_mask_list):
            
            img = img2.copy()
            for ii in range(1): # for each of the 4 masks
                color_mask = np.zeros(img2.shape)
                # print('s0 :',img2.shape[0])
                # print('s1 :',img2.shape[1])
                temp_mask = np.ones([img2.shape[0],img2.shape[1]])*127./255.
                temp_mask[img_mask_list[ii] == 0] = 0
                color_mask[:,:,ii] = temp_mask
                # if ii < 3: # use different color for each mask
                #     color_mask[:,:,ii] = temp_mask
                # else:
                #     color_mask[:,:,0],color_mask[:,:,1],color_mask[:,:,2] = temp_mask,temp_mask,temp_mask # broadcasting to 3 channels
            
                img += color_mask
                
            return img

        def show_image(image,title=None,figsize=None):
            
            if figsize is not None:
                fig = plt.figure(figsize=figsize)
                
            if image.ndim == 2:
                print("2")
                plt.imshow(image,cmap='gray')
            else:
                plt.imshow(image)
                
            if title is not None:
                plt.title(title)
                

        def show_Nimages(imgs,tags,size=(5,4),scale=1):

            N=len(imgs)
            fig = plt.figure(figsize=size)
            for i, img in enumerate(imgs):
                ax = fig.add_subplot(1, N, i + 1, xticks=[], yticks=[])
                show_image(img,title=tags[i])
            plt.show()


        NN=2
        # image_label_list=test_df.Image_Label.tolist()
        # image_id_list=[]
        images_list = []

        store_list=[]

        # for i in image_label_list:
        #     image_id_list.append(i.split('_')[0])
            
        # temp_image_id_list=list(set(image_id_list))  

        # images_list = random.sample(temp_image_id_list, 2)
        # print(images_list)
       
        images_list.append('0035ae9.jpg')
        images_list.append('006440a.jpg')

        current_batch = images_list[0: NN]

        for i, image_name in enumerate(current_batch):
   
            # p=f'{path}/train_images/'
            p='dataset/img/'
            path_im=p+image_name; 
            img = cv2.imread(path_im).astype(np.float32)
            store_list.append(img)
            #keep this
            print(img.max(), img.min()) 
            img = img/255.
            
            img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img2 = cv2.resize(img2, (525, 350),
                    interpolation = cv2.INTER_NEAREST)
            img2 = np.stack([img2,img2,img2],axis=-1)
            
            img_mask=[]
            temp_mask=[]
            img_mask_convex=[]
            img_mask_convex_minsize=[]
            container=[]
            tag=[]
            
            # show_Nimages([img2],['original_image '+str(i+1)],size=(5,4)) 

            for class_id in range(4):
                img_mask.append(make_mask(test_df, image_name + '_' + class_names[class_id],shape=(350,525)))
                temp_mask=[make_mask(test_df, image_name + '_' + class_names[class_id],shape=(350,525))]
                img_temp=draw_one_mask(img2,temp_mask)
                # container.append(img_temp)
                img_t=img_temp*255.
                store_list.append(img_t)
                # tag.append(class_names[class_id])

            # show_Nimages(container,tag,size=(20,16))
            img3 = draw_masks(img2,img_mask)
            img3_t=img3*255.
            store_list.append(img3_t)
        
            # show_Nimages([img3],['full_mask_image '+str(i+1)],size=(5,4))

            

        return store_list



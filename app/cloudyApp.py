from tkinter import *
from tkinter import ttk
from tkinter.font import BOLD
from PIL import Image
from PIL import ImageTk
from tkinter import filedialog
from cloudyCreator import *
import time
from displayMask import *

root= Tk()
root.title('Scrollar')
root.geometry("500x400")

################################################
fname ='11'

def selectFolder() :
    global fname
    root.foldername=filedialog.askdirectory()
    fname=root.foldername
    canvas.create_text(300,175, text=fname+' Folder Selected', font=("Helvetica",15))

def analyze() : 
    if fname!='11':
        out_rec = canvas.create_rectangle(1105,170,1305,195,outline = "black",width = 2)
        fill_rec = canvas.create_rectangle(1105,170,1105,195,outline = "",width = 0,fill = "green")
        for i in range(0,101):
            time.sleep(0.05)
            canvas.coords(fill_rec, (1105,170,1105+i,195))
            root.update()

        print('start test')

        object=CloudyCreator()
        object.fun()

        print('test')
                
        for i in range(101,201):
            time.sleep(0.05)
            canvas.coords(fill_rec, (1105,170,1105+i,195))
            root.update()

        canvas.coords(fill_rec, (0,0,0,0))
        canvas.coords(out_rec, (0,0,0,0))
        root.update()
        canvas.create_text(1200,175, text='Successful!', font=("Helvetica",15))

def drawCanvas(input_img, x, y, title):
        i_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)
        # Use PIL (Pillow) to convert the NumPy ndarray to a PhotoImage
        convert_img=Image.fromarray(i_img.astype('uint8'), 'RGB')

        resized_img=convert_img.resize((300,200),Image.ANTIALIAS)
        final_image = ImageTk.PhotoImage(resized_img)

        canvas2 = Canvas(canvas,height=230,width=298, bg='MistyRose2')
        canvas2.image = final_image  # <--- keep reference of your image
        canvas2.create_image(0,0,anchor='nw',image=final_image)
        canvas2.create_text(150,215, text=title, font=("Helvetica",11))
        canvas2.pack()

        canvas.create_window((x,y),window=canvas2, anchor=NW)


def showGraph():
    check=os.path.exists('./circularGraph.png')
    if check:
        print('inside')
        
       
        img = Image.open("circularGraph.png")
        filename = ImageTk.PhotoImage(img)

        canvas1 = Canvas(canvas,height=550,width=600,)
        canvas1.image = filename  # <--- keep reference of your image
        canvas1.create_image(0,0,anchor='nw',image=filename)
        canvas1.pack()

        # frame= Frame(canvas)
        canvas.create_window((500,400),window=canvas1, anchor=NW)
        # Label(frame, image=filename)

        ##########################################################

        displayObject= Display()

        image_store=displayObject.function()

        ##########################################################

        canvas.create_text(770,1000, text='Sample 01', font=("Helvetica",15),fill='cyan')

        original_img=image_store[0]
        drawCanvas(original_img,370,1050,'Original Image')

        full_img=image_store[5]
        drawCanvas(full_img,910,1050, 'All Clouds')

        fish_img=image_store[1]
        drawCanvas(fish_img,60,1300, 'Fish Clouds')

        flower_img=image_store[2]
        drawCanvas(flower_img,420,1300, 'Flower Clouds')

        sugar_img=image_store[3]
        drawCanvas(sugar_img,780,1300, 'Sugar Clouds')

        gravel_img=image_store[4]
        drawCanvas(gravel_img,1140,1300, 'Gravel Clouds')

        ########################################################

        ##########################################################

        canvas.create_text(770,1680, text='Sample 02', font=("Helvetica",20, BOLD), fill='cyan')

        ##########################################################

        original_img=image_store[0]
        drawCanvas(original_img,370,1700,'Original Image')

        ########################################################
        full_img=image_store[5]
        drawCanvas(full_img,910,1700, 'All Clouds')

        ########################################################

        fish_img=image_store[1]
        drawCanvas(fish_img,60,1950, 'Fish Clouds')

        ########################################################

        flower_img=image_store[2]
        drawCanvas(flower_img,420,1950, 'Flower Clouds')

        ########################################################

        sugar_img=image_store[3]
        drawCanvas(sugar_img,780,1950, 'Sugar Clouds')
        
        ########################################################

        gravel_img=image_store[4]
        drawCanvas(gravel_img,1140,1950, 'Gravel Clouds')

        ########################################################

        

        

      
    else:
        print('outside')


#################################################

main_frame=Frame(root)
main_frame.pack(fill=BOTH,expand=1)

my_canvas=Canvas(main_frame)
my_canvas.pack(side=LEFT, fill=BOTH, expand=1)

my_scrollarbar= ttk.Scrollbar(main_frame,orient=VERTICAL, command=my_canvas.yview)
my_scrollarbar.pack(side=RIGHT,fill=Y)

my_canvas.configure(yscrollcommand=my_scrollarbar.set)
my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion=my_canvas.bbox("all")))

second_frame= Frame(my_canvas)

my_canvas.create_window((0,0),window=second_frame, anchor=NW)

##################################################################

resizedImage=Image.open('bg10.jpg')
resizedImage=resizedImage.resize((1900,2300),Image.ANTIALIAS)
bg=ImageTk.PhotoImage(resizedImage)

sbtnImg=PhotoImage(file='select.png')
icldImg=PhotoImage(file='button_identify-clouds.png')
anyImg=PhotoImage(file='button_analyze-data.png')

canvas=Canvas(second_frame,width=1900,height=2300)
canvas.pack(fill='both',expand=True)

# set image
canvas.create_image(0,0,image=bg,anchor=NW)

buttonSelect=Button(root, image=sbtnImg, command=selectFolder, borderwidth=10)
btnS_window=canvas.create_window(300, 100, anchor=CENTER, window=buttonSelect)

buttonSelect=Button(root, image=icldImg, command=analyze, borderwidth=10)
btnS_window=canvas.create_window(1200, 100, anchor=CENTER, window=buttonSelect)

btnAnalyze=Button(root, image=anyImg, command=showGraph, borderwidth=10)
btnA_window=canvas.create_window(750, 300, anchor=CENTER, window=btnAnalyze)



root.mainloop()